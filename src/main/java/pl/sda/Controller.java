package pl.sda;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;

import javax.imageio.ImageIO;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;


public class Controller {

    public static final int IMAGE_X_POSITION = 50;
    public static final int IMAGE_Y_POSITION = 50;
    public static final int IMAGE_W_SIZE = 100;
    public static final int IMAGE_H_SIZE = 300;
    @FXML
    private Canvas canvas;
    @FXML
    private Button button;
    @FXML
    private TextField mainText;
    @FXML
    private TextField secondaryText;

    public void initialize() throws FileNotFoundException {
        final GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
    }


    public void saveImage(ActionEvent actionEvent) throws IOException {
        File fileToSaveTo = prepareFileChooserDialog().showSaveDialog(null);

        WritableImage writableImage = new WritableImage((int) canvas.getWidth(), (int) canvas.getHeight());
        canvas.snapshot(null, writableImage);
        RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);
        ImageIO.write(renderedImage, "png", fileToSaveTo);

    }

    public void openImage(ActionEvent actionEvent) throws FileNotFoundException {
        FileChooser fileChooser = prepareFileChooserDialog();
        File selectedFile = fileChooser.showOpenDialog(null);

        final GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
        gc.drawImage(new Image(new FileInputStream(selectedFile)),
                IMAGE_X_POSITION, IMAGE_Y_POSITION, IMAGE_W_SIZE, IMAGE_H_SIZE);
        gc.setStroke(Color.WHITE);
        gc.setLineWidth(3);
        gc.strokeRect(IMAGE_X_POSITION, IMAGE_Y_POSITION, IMAGE_W_SIZE, IMAGE_H_SIZE);

        gc.setFill(Color.WHITE);
        gc.setFont(Font.font("Comic Sans", FontWeight.BOLD, FontPosture.ITALIC, 30));
        gc.fillText(mainText.getText(), IMAGE_X_POSITION, IMAGE_Y_POSITION + IMAGE_H_SIZE + 50);
        gc.setFont(Font.font("Comic Sans", FontWeight.BOLD, FontPosture.ITALIC, 20));
        gc.fillText(secondaryText.getText(), IMAGE_X_POSITION, IMAGE_Y_POSITION + IMAGE_H_SIZE + 100);
        gc.setFont(Font.font("Comic Sans", FontWeight.BOLD, FontPosture.ITALIC, 5));
        gc.setFill(Color.GRAY);
        gc.fillText(System.getenv("moja_nazwa"), IMAGE_X_POSITION, IMAGE_Y_POSITION + IMAGE_H_SIZE + 120);
    }

    private FileChooser prepareFileChooserDialog() {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("JPG (*.jpg)", "*.jpg");
        fileChooser.getExtensionFilters().add(extFilter);
        return fileChooser;
    }
}
